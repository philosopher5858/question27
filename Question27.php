

<!DOCTYPE html>
<html lang="en">
<head>
   <style>
 .header{
   position:absolute;
   width: 980px;
   height: 350px;
   bottom: 40%;
   border-radius: 10px;
   padding: 40px;
   font-size: 17px;
   box-sizing: border-box;
   background:#ecf0f3;
   box-shadow: 14px 14px 20px #cbced1, -14px -14px 20px white;
   letter-spacing: 2px;
   border-left-style: outset;
   border-left-color: gold;
   border-right-style: inset;
   border-right-color: gold;
   
 }
 .header2{
   position:absolute;
   width: 480px;
   height: 350px;
   bottom: 5%;
   left: 60%;
   border-radius: 10px;
   padding: 40px;
   font-size: 17px;
   box-sizing: border-box;
   background:#ecf0f3;
   border-radius: 10;
   border-right-style: inset;
   border-right-color: gold;
   border-bottom-style: inset;
   border-bottom-color: gold;
   box-shadow: 14px 14px 20px #cbced1, -14px -14px 20px white;
   letter-spacing: 2px;
    animation:bouncing
    1s infinite alternate;   
 }
 @keyframes bouncing{
    0%{
        transform: translate3d(50px,150px,800px);
    }
/* 0%{
    transform: translateY(300px) ;
}
100%{
    transform: translateX(100px);
} */
 }

.header4 {
    border-radius: 10px;
   padding: 40px;
   font-size: 17px;
   box-sizing: border-box;
   background:#ecf0f3;
   border-radius: 10;
   border-right-style: inset;
   border-right-color: gold;
   border-bottom-style: inset;
   border-bottom-color: gold;
   box-shadow: 14px 14px 20px #cbced1, -14px -14px 20px white;
   letter-spacing: 2px;
    animation: animationRightLeft 2s ease-in-out infinite;
    
}
  .animationLeftRight {
    animation: animationLeftRight 1s ease-in-out infinite;
  }
  .tans3s {
    animation: animationLeftRight 3s ease-in-out infinite;
  }
  .tans4s {
    animation: animationLeftRight 4s ease-in-out infinite;
  }
  
  @keyframes animationRightLeft {
    0% {
      transform: translateX(100px);
    }
    50% {
      transform: translateX(-100px);
    }
    100% {
      transform: translateX(100px);
    }
  } 
  
  @keyframes animationRightLeft {
    0% {
      transform: translateX(920px);
    }
    50% {
      transform: translateX(-80px);
    }
    100% {
      transform: translateX(920px);
    }
  } 
 
 .header3{
   position:absolute;
   width: 480px;
   height: 350px;
   top: 40%;
   right: 60%;
   border-radius: 10px;
   padding: 40px;
   font-size: 17px;
   box-sizing: border-box;
   background:#ecf0f3;
   border-left-style: outset;
   border-left-color:gold;
   border-bottom-style: inset;
   border-bottom-color: gold;
   box-shadow: 14px 14px 20px #cbced1, -14px -14px 20px white;
   letter-spacing: 2px;
   animation:bouncing2 1s infinite alternate;
 }
 @keyframes bouncing2{
    100%{
        transform: translate3d(150px,150px,800px);
    }
    /* 100%{
        transform: translate3d(150px,150px,150px);
    } */
 }

 
 body {
   margin: 0;
   width: 100vw;
   height: 100vh;
   background: #ecf0f3;
   display: flex;
   align-items: center;
   text-align: center;
   justify-content: center;
   place-items: center;
   overflow: hidden;
   font-family: 'Courier New', Courier, monospace;
 }
 
 .container {
   position: relative;
   width: 350px;
   height: 200px;
   border-radius: 20px;
   padding: 40px;
   top: 100px;
   right: 40px; ;
   /* box-sizing: border-box; */
   background:#ecf0f3;
   box-shadow: 14px 14px 20px #cbced1, -14px -14px 20px white;
   letter-spacing: 2px;
   border-right-style: inset;
   border-right-color: gold;
   border-left-style: outset;
   border-left-color: gold;
   
 }
 
 .inputs {
   text-align: left;
   margin-top: 3px;
 }
 
 label, input, button {
   display:inline;
   width: 100%;
   padding: 0;
   border: none;
   outline: none;
   box-sizing: border-box;
 }
 .lable2{
   margin-bottom: 40px;
   padding-right: 100%;
   font-family: 'Courier New', Courier, monospace;
 }
 
 label {
   margin-bottom: 40px;
   padding-right: 100%;
   font-family: 'Courier New', Courier, monospace;
 }
 
 label:nth-of-type(2) {
   margin-top: 12px;
   padding-right: 0%;
 }
 
 input::placeholder {
   color: gray;
 }
 
 input {
   background: #f7f9fb;
   padding: 1px;
   padding-left: 10px;
   height: 35px;
   font-size: 14px;
   border-radius: 10px;
   box-shadow: inset 6px 6px 6px #7da9d6, inset -6px -6px 6px rgb(205, 246, 249);
 }
 
 button {
   color: white;
   margin-top: 20px;
   background: gold;
   height: 40px;
   width: 220px;
   border-radius: 20px;
   cursor: pointer;
   font-weight: 900;
   box-shadow: 6px 6px 6px #cbced1, -6px -6px 6px white;
   transition: 0.5s;
   right: 45px;
 }
 
 button:hover {
   box-shadow: none;
 }
 
 a {
   position: absolute;
   font-size: 8px;
   bottom: 4px;
   right: 4px;
   text-decoration: none;
   color: black;
   background: yellow;
   border-radius: 10px;
   padding: 2px;
 }
 
 h1 {
   position: absolute;
   top: 0;
   left: 0;
 }
 
 
   </style>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Document</title>
</head>
<body>
    
    <div class="header">
       <p> <b>Question 27.</p>WAP to calculate the percentage of a student in 5 subjects and then find his grade accordingly.<br><br> RESULT	</b>
   </div>
   <h2 class="shit"> <div class="header2"></div></h2>
   <div class="header3"></div>
   <div class="header4"></div>
   <form action="Question27.html" method="POST">
   
       <DIv class="container">
      <?php
if($_SERVER['REQUEST_METHOD']=='POST'){
    $first_mark = $_POST['FirstMark'];
    $second_mark = $_POST['SecondMark'];
    $third_mark = $_POST['ThirdMark'];
    $fourth_mark = $_POST['FourthMark'];
    $fifth_mark = $_POST['FifthMark'];

    $Total_Mark = $first_mark + $second_mark + $third_mark + $fourth_mark + $fifth_mark;
        $percentage = ((($Total_Mark)/500)*100);
            echo "PERCENTAGE"."<br><h2>".$percentage."%"."</h1><br>";
        
  


        
        if($first_mark>=75 && $first_mark<100){
          $Mark = "A";
        }elseif($first_mark>=60 && $first_mark<75){
          $Mark = "B";
        }elseif($first_mark>=50 && $first_mark<60){
          $Mark = "C";
        }elseif($first_mark>=35 && $first_mark<45){
          $Mark = "D";
        }elseif($first_mark>=25 && $first_mark<35){
          $Mark = "E";
        }elseif($first_mark>=0 && $first_mark<25){
          $Mark = "F";
        }else{
          $Mark = "Invalid Mark";
        }

      if($second_mark>=75 && $second_mark<100){
        $Mark2 = "A";
      }elseif($second_mark>=60 && $second_mark<75){
        $Mark2 = "B";
      }elseif($second_mark>=50 && $second_mark<60){
        $Mark2 = "C";
      }elseif($second_mark>=35 && $second_mark<45){
        $Mark2 = "D";
      }elseif($second_mark>=25 && $second_mark<35){
        $Mark2 = "E";
      }elseif($second_mark>=0 && $second_mark<25){
        $Mark2 = "F";
      }else{
        $Mark2 = "Invalid Mark";
      }
            
      if($third_mark>=75 && $third_mark<100){
        $Mark3 = "A";
      }elseif($third_mark>=60 && $third_mark<75){
        $Mark3 = "B";
      }elseif($third_mark>=50 && $third_mark<60){
        $Mark3 = "C";
      }elseif($third_mark>=35 && $third_mark<45){
        $Mark3 = "D";
      }elseif($third_mark>=25 && $third_mark<35){
        $Mark3 = "E";
      }elseif($third_mark>=0 && $third_mark<25){
        $Mark3 = "F";
      }else{
        $Mark3 = "Invalid Mark";
      }
      if($fourth_mark>=75 && $fourth_mark<100){
        $Mark4 = "A";
      }elseif($fourth_mark>=60 && $fourth_mark<75){
        $Mark4 = "B";
      }elseif($fourth_mark>=50 && $fourth_mark<60){
        $Mark4 = "C";
      }elseif($fourth_mark>=35 && $fourth_mark<45){
        $Mark4 = "D";
      }elseif($fourth_mark>=25 && $fourth_mark<35){
        $Mark4 = "E";
      }elseif($fourth_mark>=0 && $fourth_mark<25){
        $Mark4 = "F";
      }else{
        $Mark4 = "Invalid Mark";
      }

      if($fifth_mark >=75 && $fifth_mark <100){
        $mark5 = "A";
      }elseif($fifth_mark >=60 && $fifth_mark <75){
        $mark5 = "B";
      }elseif($fifth_mark >=50 && $fifth_mark <60){
        $mark5 = "C";
      }elseif($fifth_mark >=35 && $fifth_mark <45){
        $mark5 = "D";
      }elseif($fifth_mark >=25 && $fifth_mark <35){
        $mark5 = "E";
      }elseif($fifth_mark >=0 && $fifth_mark <25){
        $mark5 = "F";
      }else{
        $mark5 = "Invalid Mark";      }

        
      echo "<b>Subject 1: Mark: ".$first_mark." Grade: ".$Mark."<br>";
      echo "<b>Subject 2: Mark: ".$second_mark." Grade: ".$Mark2."<br>";
      echo "<b>Subject 3: Mark: ".$third_mark." Grade: ".$Mark3."<br>";
      echo "<b>Subject 4: Mark: ".$fourth_mark." Grade: " .$Mark4."<br>";
      echo "<b>Subject 5: Mark: ".$fifth_mark." Grade: " .$mark5."<br>";

    }
      ?>
             
             <button type="submit">GO BACK</button> 
       </DIv>
      
   </form>
</body>
</html>